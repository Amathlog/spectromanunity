﻿using UnityEngine;
using System.Collections;
using System.Globalization;

public class PlatformController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;           // Indicates if the player is facing right
    [HideInInspector] public bool jump = false;                 // Is the player is jumping ?
    [HideInInspector] public bool freeze = false;               // Is the player is frozen ?
    [HideInInspector] public GameObject activePlatform;        // Current plateform the player stands on
    public float moveForce = 365f;                              // The force applicate in moving horizontaly
    public float maxSpeed = 5f;                                 // The Max speed of the player
    public float jumpForce = 1000f;                             // The force applicate when jumping
    public Transform groundCheck;                               // Indicator to the gorundCheck
    [HideInInspector] public bool colliding;

    private bool grounded = false;                              // Is the player on the ground ?
    private Animator anim;                                      // Reference to the animator attached to the player
    private Rigidbody2D rb2d;                                   // Reference to the rigidBody2D attached to the player
    private ColorManager colorManager;                          // Reference to the colorManager attached to the player
    private Shooter shooter;                                    // Reference to the shooter attached to the player
    private Vector3 activeLocalPlatformPoint;
    private Vector3 activeGlobalPlatformPoint;



    // Use this for initialization
    void Awake (){
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        colorManager = GetComponent<ColorManager>();
        shooter = GetComponent<Shooter>();
        freeze = false;
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (!col.gameObject.CompareTag("Platform")) {
            colliding = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
        // If the player can move
	    if (!freeze) {
            RaycastHit2D raycast = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
	        grounded = raycast;
	        /*if (raycast) {
	            activePlatform = (raycast.rigidbody != null && raycast.transform.position.y <= transform.position.y) ? raycast.transform.gameObject : null;
	        }
	        else {
	            activePlatform = null;
	        }*/

	        if (Input.GetButtonDown("Jump") && grounded) {
	            jump = true;
	        }

	        if (Input.GetButtonDown("Fire2") && activePlatform != null) {
	            colorManager.changeColor(activePlatform.GetComponent<PlaformColor>().color, true);
	        }

            if (Input.GetButtonDown("Fire1")) {
                shooter.fire();
            }
            print("coucou");
            
        }
	}

    // FixedUpadte is called once per frame for physics
    void FixedUpdate() {
        //If the player is colliding horizontally
        if(colliding)
            anim.SetFloat("Speed", 0);
        //If the player can move
        if (!freeze && !colliding) {
            float h = Input.GetAxis("Horizontal");
            print(h);

            anim.SetFloat("Speed", Mathf.Abs(h));

            if (h * rb2d.velocity.x < maxSpeed)
                rb2d.AddForce(Vector2.right * h * moveForce);
            if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
                rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

            if (jump) {
                jump = false;
                rb2d.AddForce(Vector2.up*jumpForce);
            }

            if (h > 0 && !facingRight || h < 0 && facingRight)
                flip();

            // If the player is standing on a moving platform, he moves with it
            if (activePlatform != null) {
                activeGlobalPlatformPoint = transform.position;
                activeLocalPlatformPoint = activePlatform.transform.InverseTransformPoint(transform.position);
                Vector3 newGlobalPlatformPoint = activePlatform.transform.TransformPoint(activeLocalPlatformPoint);
                Vector3 moveDistance = (newGlobalPlatformPoint - activeGlobalPlatformPoint);
                // Setting transform.position directly causes us to go through walls if we're on a rotating block.
                // But it's necessary to make moving platforms work.
                if (activePlatform.GetComponent<Rigidbody2D>().isKinematic && activePlatform.GetComponent<Rigidbody2D>().velocity.sqrMagnitude > 0.0)
                {
                    // Moving platform. Change the position directly so the character
                    // won't fall through the platform.
                    transform.position = transform.position + moveDistance;
                }
                else
                {
                    // Store the desired movement for use in CharacterController.Move.
                    //platformMovementOffset = moveDistance;
                }
            }
        }
    }

    // Change the orientation of the player
    private void flip() {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
