﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    
    private CharacterController2D controller;
    private float normalizedHorizontalSpeed;
    private ColorManager colorManager;
    private Animator anim;
    private Vector3 deathPosition;
    private Shooter shooter;

    public bool isFacingRight { get; private set; }
    public float maxSpeed = 3f;
    public float speedAccelerationOnGround = 10f;
    public float speedAccelerationInAir = 5f;

    public void Start() {
        controller = GetComponent<CharacterController2D>();
        colorManager = GetComponent<ColorManager>();
        anim = GetComponent<Animator>();
        shooter = GetComponent<Shooter>();
        isFacingRight = transform.localScale.x > 0;
    }

    public void Update() {
        if (!controller.State.Freeze) {
            handleInput();

            float moveFactor = controller.State.isGrounded ? speedAccelerationOnGround : speedAccelerationInAir;
            controller.SetHorizontalForce(Mathf.Lerp(controller.Velocity.x, normalizedHorizontalSpeed*maxSpeed,
                Time.deltaTime*moveFactor));

            handleAnimation();
        }
        else {
            if(transform.position.y - deathPosition.y < 0)
                GameObject.Find("GameManager").GetComponent<GameManager>().ReloadingLevel();
            else
                controller.SetHorizontalForce(0f);
        }
    }

    private void handleInput() {
        float h = Input.GetAxis("Horizontal");
        if (!(Mathf.Abs(h) > 0.1))
            h = 0;

        if (h < 0) { //Left
            normalizedHorizontalSpeed = -1;
            if (isFacingRight) flip();
        } else if (h > 0) { // Right
            normalizedHorizontalSpeed = 1;
            if (!isFacingRight) flip();
        }
        else { // Idle
            normalizedHorizontalSpeed = 0;
        }

        if (Input.GetButtonDown("Jump") && controller.CanJump) {
            controller.Jump();
        }

        if (Input.GetButtonDown("Fire2") && controller.StandingOn != null && controller.StandingOn.CompareTag("Platform"))
        {
            colorManager.changeColor(controller.StandingOn.GetComponent<PlaformColor>().color, true);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            shooter.fire();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().PauseAction();
        }

    }

    private void handleAnimation() {
        if (Mathf.Abs(controller.Velocity.x) > 0.1f) {
            anim.Play("Move" + colorManager.getstringColor(colorManager.getCurrentColor()));
        } else if (Mathf.Abs(controller.Velocity.x) < 0.1f) {
            anim.Play("Idle" + colorManager.getstringColor(colorManager.getCurrentColor()));
        }
    }

    // Change the orientation of the player
    private void flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    // Method called from the outside to kill the player
    public void death()
    {
        //Not going two times in the function
        if (!controller.State.Freeze)
        {
            controller.State.Freeze = true;
            
            //Animation section
            anim.SetFloat("Speed", 1.0f);
            anim.Play("Death");

            // Freeze the camera and the player, not moving anymore
            GameObject.Find("Main Camera").GetComponent<CameraController>().freeze = true;


            //Not handling 
            controller.HandleCollision = false;

            //Set the velocity to Zero, note the deathPosition and launch the player in the air
            controller.SetForce(Vector2.zero);
            transform.position -= Vector3.forward;
            deathPosition = transform.position + Vector3.down * 7.0f;
            controller.AddForce(new Vector2(0,controller.Parameters.jumpForce));
        }
    }

    public float GetVelocityX() {
        return controller.Velocity.x;
    }
}
