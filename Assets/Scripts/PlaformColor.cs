﻿using UnityEngine;
using System.Collections;
using System;

public class PlaformColor : SwitchActivator
{

    public Colors color;                        //Enumeration of colors, setting the color of the platform
    public Transform[] waypoints;               //Waypoints, path of the moving platform

    public enum Looping {                       // 3 differents states :
        NONE,                                   // Stop at the end of the path
        LOOP,                                   // Restart at the begining of the path
        BACKWARD                                // Goes backwards
    };

    public Looping loop = Looping.BACKWARD;     // State defined in the controller
    public float speed = 3.0f;                  // Speed of the platform
    public bool moving = false;                 // Is the platform moving ?

    private int currentWaypoint = 0;            // The current position in the path
    private int increment = 1;                  // The increment add when a waypoint is reached (useful in backwards because set to -1)

	// Use this for initialization
	void Start () {
    }

    // To remove... Set the color of the platform to the player on collision
    
    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.CompareTag("Player")) {
            if (col.contacts[0].normal.y <= -0.9f) {
                col.gameObject.GetComponent<PlatformController>().activePlatform = gameObject;
                col.gameObject.GetComponent<PlatformController>().colliding = false;
            }
            else if(col.contacts[0].normal.y >= 0.9f){
                col.gameObject.GetComponent<PlatformController>().colliding = false;
            }
            else {
                col.gameObject.GetComponent<PlatformController>().colliding = true;
                col.gameObject.GetComponent<PlatformController>().activePlatform = null;
            }
        }
    }

    void OnCollisionExit2D(Collision2D col) {
        if (col != null)
        {
            var controller = col.gameObject.GetComponent<PlatformController>();
            if (controller != null)
                controller.activePlatform = null;
        }
        
    }

    void FixedUpdate() {
        if (moving) {
            if (currentWaypoint >= waypoints.Length) {
                if (loop == Looping.LOOP) {
                    currentWaypoint = 0;
                }
                else if (loop == Looping.BACKWARD) {
                    currentWaypoint = waypoints.Length - 2;
                    increment = -1;
                }
                else {
                    moving = false;
                }
            }
            else if (currentWaypoint < 0) {
                if (loop == Looping.NONE) {
                    moving = false;
                }
                currentWaypoint = 1;
                increment = 1;
            }
            else {
                move();
            }
        }
        else {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }

    // Moving function
    private void move()
    {
        Vector3 target = waypoints[currentWaypoint].position;
        Vector3 moveDirection = target - transform.position;

        if (moveDirection.magnitude < 0.1)
        {
            currentWaypoint += increment;
        }
        else {
            GetComponent<Rigidbody2D>().velocity = moveDirection.normalized*speed;
            //GetComponent<Rigidbody2D>().MovePosition(transform.position + moveDirection.normalized * speed * Time.fixedDeltaTime);
        }
    }

    // The Gizmo, useful to see the path in the editor
    public void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        for (int i = 0; i < waypoints.Length - 1; i++) {
            Gizmos.DrawLine(waypoints[i].position, waypoints[i+1].position);
        }
    }

    
    public override void onActivation(bool state) {
        moving = state;
    }
}
