﻿using UnityEngine;
using System.Collections;

public class WallController : SwitchActivator {

    public bool disapear = false;       // If the wall just desapear when switch is activated or not
    public Transform target;            // The target to reach if the wall doesn't desapear
    public float speed = 3.0f;          // The speed of the animation

    private bool firstTrigger = false;
    private Vector3 targetVector;


	// Use this for initialization
	void Start () {
        targetVector = target.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void onActivation(bool state) {
        if (disapear)
            gameObject.SetActive(!state);
        else {
            if (!firstTrigger && !state)
                firstTrigger = true;
            else {
                firstTrigger = true;
                Vector3 saveTarget = transform.position;
                iTween.MoveTo(gameObject, iTween.Hash("easeType", "linear", "speed", speed, "position", targetVector));
                targetVector = saveTarget;
            }
        }
    }
}
