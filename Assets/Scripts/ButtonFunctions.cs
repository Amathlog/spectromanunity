﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour {

    public void LoadScene(int level) {
        SceneManager.LoadScene(level);
    }

    public void Exit() {
        Application.Quit();
    }

    public void Fullscreen() {
        Screen.fullScreen = !Screen.fullScreen;
    }
}
