﻿using UnityEngine;
using System.Collections;

public class CharacterController2D : MonoBehaviour {

    //Const 
    private const float Skinwidth = 0.02f;                                                  // Width of the skin
    private const int Totalhorizontalrays = 4;                                              // Number of rays horizontally
    private const int Totalverticalrays = 8;                                                // Number of rays vertically

    private static readonly float SlopeLimitTangent = Mathf.Tan(75f*Mathf.Deg2Rad);         // The Limit of the tangent

    // Unity Editable Parameters
    public LayerMask PlatformMask;                                                          // The colliding layer
    public ControllerParams2D DefaultParams;                                                // Parameters
    public ControllerStates2D State { get; private set; }                                   // Current state

    //Properties of this class
    public Vector2 Velocity {get { return _velocity; } }                                    // Access to the velocity

    public bool CanJump {                                                                   // Access to the possibility of jumping
        get {
            if(Parameters.jumpController == ControllerParams2D.JumpBehavior.CanJumpAnywhere)
                return _jumpIn < 0;
            if (Parameters.jumpController == ControllerParams2D.JumpBehavior.CanJumpOnTheGround)
                return State.isGrounded;
            return false;
        }
    }                                                                                       
    public bool HandleCollision { get; set; }                                               // Are we handling collisions
    public ControllerParams2D Parameters
                { get { return _overrideParams ?? DefaultParams; } }                        // The current parameters (default or overrided)
    public GameObject StandingOn { get; set; }
    public Vector3 PlatformVelocity { get; private set; }

    // Storage
    private Vector2 _velocity;                                                              // Current velocity
    private Transform _transform;                                                           // Current transform (faster ?)
    private Vector3 _localScale;                                                            // Current localScale
    private BoxCollider2D _boxCollider;                                                     // Reference to the attached boxCollider2D
    private ControllerParams2D _overrideParams = null;                                      // If there is overrided parameters, which ones
    private float _jumpIn;
    private Vector3 _activeGlobalPlatformPoint;
    private Vector3 _activeLocalPlatformPoint;
    private GameObject _lastStandingOn;

    // Raycast variables
    private float _verticalDistanceBetweenRays;                                             // The distance between rays vertically
    private float _horizontalDistanceBetweenRays;                                           // The distance between rays horizontally
    private Vector3 _raycastTopLeft;                                                        // TopLeft point of our collider
    private Vector3 _raycastBottomRight;                                                    // BottomRight point of our collider
    private Vector3 _raycastBottomLeft;                                                     // BottomLeft point of our collider
    

    public void Awake() {
        HandleCollision = true;
        State = new ControllerStates2D();
        State.Freeze = false;
        _transform = transform;
        _localScale = transform.localScale;
        _boxCollider = GetComponent<BoxCollider2D>();

        //Initialization of distance between rays
        _horizontalDistanceBetweenRays = (_boxCollider.size.x * Mathf.Abs(_localScale.x) - 2 * Skinwidth) / (Totalhorizontalrays -1);
        _verticalDistanceBetweenRays = (_boxCollider.size.y * Mathf.Abs(_localScale.y) - 2 * Skinwidth) / (Totalverticalrays - 1);
    }

    public void AddForce(Vector2 force) {
        _velocity += force;
    }

    public void SetForce(Vector2 force) {
        _velocity = force;
    }

    public void SetHorizontalForce(float x) {
        _velocity.x = x;
    }

    public void SetVerticalForce(float y) {
        _velocity.y = y;
    }

    public void Jump() {
        AddForce(new Vector2(0, Parameters.jumpForce));
        _jumpIn = Parameters.jumpFrequency;
    }

    public void LateUpdate() {
        _jumpIn -= Time.deltaTime;
        _velocity.y -= Parameters.gravity*Time.deltaTime;

        Move(Velocity*Time.deltaTime);
    }

    private void Move(Vector2 deltaMovement) {
        var wasGrounded = State.isGrounded;
        State.reset();

        //If we have handled collison, we move
        if (HandleCollision) {
            HandlePlatform();
            CalculateRayOrigins();


            if(deltaMovement.y < 0 && wasGrounded)
                HandleVerticalSlope(ref deltaMovement);
            if (Mathf.Abs(deltaMovement.x) > 0.001f)
                MoveHorizontally(ref deltaMovement);

            MoveVertically(ref deltaMovement);

            //For moving platforms
            CorrectHorizontalPlacement(ref deltaMovement, true);
            CorrectHorizontalPlacement(ref deltaMovement, false);
        }

        _transform.Translate(deltaMovement, Space.World);

        //Set our new velocity
        if (Time.deltaTime > 0) {
            _velocity = deltaMovement/Time.deltaTime;
        }

        //checking not overpass the maxSpeed
        _velocity.x = Mathf.Min(_velocity.x, Parameters.maxVelocity.x);
        _velocity.y = Mathf.Min(_velocity.y, Parameters.maxVelocity.y);

        if (State.isMovingUpSlope)
            _velocity.y = 0;

        if (StandingOn != null) {
            _activeGlobalPlatformPoint = transform.position;
            _activeLocalPlatformPoint = StandingOn.transform.InverseTransformPoint(transform.position);

            if (_lastStandingOn != StandingOn) {
                if (_lastStandingOn != null)
                    _lastStandingOn.SendMessage("ConrollerExit2D", this, SendMessageOptions.DontRequireReceiver);

                StandingOn.SendMessage("ControllerEnter2D", this, SendMessageOptions.DontRequireReceiver);
                _lastStandingOn = StandingOn;
            }
            else if (StandingOn != null) {
                StandingOn.SendMessage("ControllerStay2D", this, SendMessageOptions.DontRequireReceiver);
            }
        }
        else if (_lastStandingOn != null){
            _lastStandingOn.SendMessage("ConrollerExit2D", this, SendMessageOptions.DontRequireReceiver);
            _lastStandingOn = null;
        }
    }

    private void HandlePlatform() {
        if (StandingOn != null) {
            var newGlobalPlatformPoint = StandingOn.transform.TransformPoint(_activeLocalPlatformPoint);
            var moveDistance = newGlobalPlatformPoint - _activeGlobalPlatformPoint;

            if(moveDistance != Vector3.zero)
                transform.Translate(moveDistance, Space.World);

            PlatformVelocity = (newGlobalPlatformPoint - _activeGlobalPlatformPoint)/Time.deltaTime;
        }
        else {
            PlatformVelocity = Vector3.zero;
        }

        StandingOn = null;

    }

    private void CorrectHorizontalPlacement(ref Vector2 deltaMovement, bool isRight) {
        var halfWidth = _boxCollider.size.x*_localScale.x/2f;
        var rayOrigin = isRight ? _raycastBottomRight : _raycastBottomLeft;
        if (isRight)
            rayOrigin.x -= (halfWidth - Skinwidth);
        else {
            rayOrigin.x += (halfWidth - Skinwidth);
        }

        var rayDirection = isRight ? Vector2.right : Vector2.left;
        var offset = 0f;

        for (var i = 1; i < Totalverticalrays - 1; ++i) {
            var rayVector = new Vector2(deltaMovement.x + rayOrigin.x,
                deltaMovement.y + rayOrigin.y + (i*_verticalDistanceBetweenRays));
            //Debug.DrawRay(rayVector, rayDirection*halfWidth, isRight ? Color.cyan : Color.magenta);

            var raycastHit = Physics2D.Raycast(rayVector, rayDirection, halfWidth, PlatformMask);

            if (!raycastHit)
                continue;

            offset = isRight ? (raycastHit.point.x - _transform.position.x - halfWidth) : halfWidth - (_transform.position.x - raycastHit.point.x);
        }

        deltaMovement.x += offset;
    }

    private void CalculateRayOrigins() {
        var size = new Vector2(_boxCollider.size.x * Mathf.Abs(_localScale.x), _boxCollider.size.y * Mathf.Abs(_localScale.y))/2.0f;
        var center = new Vector2(_boxCollider.offset.x * _localScale.x, _boxCollider.offset.y * _localScale.y);
        _raycastTopLeft = _transform.position +
                          new Vector3(center.x - size.x + Skinwidth, center.y + size.y - Skinwidth);
        _raycastBottomRight = _transform.position +
                              new Vector3(center.x + size.x - Skinwidth, center.y - size.y + Skinwidth);
        _raycastBottomLeft = _transform.position +
                              new Vector3(center.x - size.x + Skinwidth, center.y - size.y + Skinwidth);
    }

    private void MoveHorizontally(ref Vector2 deltaMovement) {

        //Raycast initialisation
        var isGoingRight = deltaMovement.x > 0;
        var rayDistance = Mathf.Abs(deltaMovement.x) + Skinwidth;
        var rayDirection = isGoingRight ? Vector2.right : Vector2.left;
        var rayOrigin = isGoingRight ? _raycastBottomRight : _raycastBottomLeft;

        //Loop on raycasts
        for (var i = 0; i < Totalverticalrays; ++i) {
            var rayVector = new Vector2(rayOrigin.x, rayOrigin.y + (i * _verticalDistanceBetweenRays));
            Debug.DrawRay(rayVector, rayDirection*rayDistance, Color.red);

            var raycastHit = Physics2D.Raycast(rayVector, rayDirection, rayDistance, PlatformMask);

            //If nothing hit, continue the loop
            if(!raycastHit)
                continue;

            //For slopes, see after
            if (i == 0 &&
                HandleHorizontalSlope(ref deltaMovement, Vector2.Angle(raycastHit.normal, Vector2.up), isGoingRight))
                break;

            //Cannot go further than the colliding point
            deltaMovement.x = raycastHit.point.x - rayVector.x;
            rayDistance = Mathf.Abs(deltaMovement.x);

            //Update state machine
            if (isGoingRight) {
                deltaMovement.x -= Skinwidth;
                State.isCollidingRight = true;
            }
            else {
                deltaMovement.x += Skinwidth;
                State.isCollidingLeft = true;
            }

            //If something messed up
            if (rayDistance < Skinwidth + 0.001f)
                break;
        }
    }

    private void MoveVertically(ref Vector2 deltaMovement) {
        var isGoingUp = deltaMovement.y > 0;
        var rayDistance = Mathf.Abs(deltaMovement.y) + Skinwidth;
        var rayDirection = isGoingUp ? Vector2.up : Vector2.down;
        var rayOrigin = isGoingUp ? _raycastTopLeft : _raycastBottomLeft;

        //Offset applied because deltaMovement.x already computed
        rayOrigin.x += deltaMovement.x;

        var standingOnDistance = float.MaxValue;

        //Loop on raycasts
        for (var i = 0; i < Totalhorizontalrays; ++i)
        {
            var rayVector = new Vector2(rayOrigin.x + (i * _horizontalDistanceBetweenRays), rayOrigin.y);
            Debug.DrawRay(rayVector, rayDirection * rayDistance, Color.red);

            var raycastHit = Physics2D.Raycast(rayVector, rayDirection, rayDistance, PlatformMask);

            //If nothing hit, continue the loop
            if (!raycastHit)
                continue;

            if (!isGoingUp) {
                var verticalDistanceToHit = _transform.position.y - raycastHit.point.y;
                if (verticalDistanceToHit < standingOnDistance) {
                    standingOnDistance = verticalDistanceToHit;
                    StandingOn = raycastHit.collider.gameObject;
                }
            }

            deltaMovement.y = raycastHit.point.y - rayVector.y;

            rayDistance = Mathf.Abs(deltaMovement.y);

            //Update state machine
            if (isGoingUp)
            {
                deltaMovement.y -= Skinwidth;
                State.isCollidingUp = true;
            }
            else
            {
                deltaMovement.y += Skinwidth;
                State.isCollidingDown = true;
            }

            // If we're on a slope
            if (!isGoingUp && deltaMovement.y > 0.0001f) {
                State.isMovingUpSlope = true;
            }

            //If something messed up
            if (rayDistance < Skinwidth + 0.001f)
                break;
        }
    }

    private void HandleVerticalSlope(ref Vector2 deltaMovement) {
        var center = (_raycastBottomRight.x + _raycastBottomLeft.x)/2;
        var direction = Vector2.down;

        var slopeDistance = SlopeLimitTangent*(_raycastBottomRight.x - center);
        var slopeVector = new Vector2(center, _raycastBottomLeft.y);

        Debug.DrawRay(slopeVector, direction * slopeDistance, Color.white);

        var raycastHit = Physics2D.Raycast(slopeVector, direction, slopeDistance, PlatformMask);

        if (!raycastHit)
            return;

        // ReSharper disable once CompareOfFloatsByEqualityOperator
        var isMovingDownSlope = Mathf.Sign(raycastHit.normal.x) == Mathf.Sign(deltaMovement.x);
        if (!isMovingDownSlope)
            return;

        var angle = Vector2.Angle(raycastHit.normal, Vector2.up);
        if (Mathf.Abs(angle) < 0.001f)
            return;
        State.isMovingDownSlope = true;
        State.slopeAngle = angle;
        deltaMovement.y = raycastHit.point.y - slopeVector.y;
    }

    private bool HandleHorizontalSlope(ref Vector2 deltaMovement, float angle, bool isGoingRight) {
        if (Mathf.RoundToInt(angle) == 90)
            return false;
        if (angle > Parameters.slopeLimit) {
            deltaMovement.x = 0;
            return true;
        }

        if (deltaMovement.y > 0.07f)
            return true;

        deltaMovement.x += isGoingRight ? -Skinwidth : Skinwidth;
        deltaMovement.y = Mathf.Abs(Mathf.Tan(angle*Mathf.Deg2Rad*deltaMovement.x));
        State.isMovingUpSlope = true;
        State.isCollidingDown = true;
        return true;
    }
        
}
