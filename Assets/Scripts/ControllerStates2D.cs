﻿using System;
using UnityEngine;
using System.Collections;

public class ControllerStates2D {

    public bool isCollidingRight { get; set; }
    public bool isCollidingLeft { get; set; }
    public bool isCollidingUp { get; set; }
    public bool isCollidingDown { get; set; }
    public bool isMovingDownSlope { get; set; }
    public bool isMovingUpSlope { get; set; }

    public bool isGrounded {
        get { return isCollidingDown; }
    }

    public float slopeAngle { get; set; }

    public bool hasCollision {
        get { return isCollidingDown || isCollidingLeft || isCollidingRight || isCollidingUp; }
    }

    public bool Freeze { get; set; }

    public void reset() {
        isCollidingRight =
            isCollidingDown =
                isCollidingLeft =
                    isCollidingUp =
                        isMovingDownSlope =
                            isMovingUpSlope = false;
        slopeAngle = 0f;
    }

    public override string ToString() {
        return
            String.Format("Controller state : r:{0}, l:{1}, u:{2}, d:{3}, slopeUp:{4}, slopeDown:{5}, angle:{6}",
            isCollidingRight,
            isCollidingLeft,
            isCollidingUp,
            isCollidingDown,
            isMovingUpSlope,
            isMovingDownSlope, 
            slopeAngle);
    }
}
