﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIText : MonoBehaviour {

    private GameManager manager;
    public Text textScore;
    public Text textTime;

    void Awake() {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

	void Update () {
        textScore.text = "Score = " + manager.Data.CurrentScore + "\nMax = " + manager.GetSceneMaxScore(manager.CurrentScene);
	    textTime.text = "Time = " + manager.TimeElapsed;
	}
}
