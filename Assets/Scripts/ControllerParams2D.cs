﻿using System;
using UnityEngine;
using System.Collections;

/**
* This class store the parameters of the 2DController
*/

[Serializable]
public class ControllerParams2D {
    
    public enum JumpBehavior {
        CanJumpOnTheGround,
        CanJumpAnywhere,
        CantJump
    }

    public Vector2 maxVelocity = new Vector2(float.MaxValue, float.MaxValue);

    [Range(0, 90)] public float slopeLimit = 40f;
    public float gravity = 24f;
    public JumpBehavior jumpController;
    public float jumpFrequency = .25f;
    public float jumpForce = 12f;
}
