﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float speed = 10f;                                   // Speed of the projectile
    public float lifeTime = 3f;                                 // Duration of the projectile before being destroy
    public Colors color;                                        // Color of the projectile
    [HideInInspector] public Vector3 direction = Vector3.right; // Give the direction of the projectile
    public Material[] materials;                                // Used to change on runtime materials for projectiles

    private Rigidbody2D rb2D;                                   // Reference to the rigidBody2D attached to the projectile
    private CameraController cameraController;                            // The camera, killing the projectile if outside the camera

    // Coroutine destroying the projectile after X seconds.
    private IEnumerator living() {
        yield return new WaitForSeconds(lifeTime);
        if(transform)
            Destroy(transform.gameObject);
    }

    // If the projectile collide anything, it's destroyed (Perhaps not good)
    void OnCollisionEnter2D(Collision2D col) {
        if(!col.gameObject.CompareTag("Mirror"))
            Destroy(transform.gameObject);
    }

	// Use this for initialization
	void Awake () {
        rb2D = GetComponent<Rigidbody2D>();
        cameraController = GameObject.FindGameObjectWithTag("MainCamera").gameObject.GetComponent<CameraController>();
	    StartCoroutine(living());
	}
	
	// Update is called once per frame
	void Update () {
        rb2D.velocity = speed * direction;
    }

    void FixedUpdate() {
        //If the projectile is outside the camera, kills it
        if(transform.position.x > cameraController.transform.position.x + cameraController.width / 2f || transform.position.x < cameraController.transform.position.x - cameraController.width / 2f
            || transform.position.y > cameraController.transform.position.y + cameraController.height / 2f || transform.position.y < cameraController.transform.position.y - cameraController.height / 2f)
            Destroy(transform.gameObject);
    }

    public void filtercolor(Colors filter) {
        color = ColorManager.filtering(color, filter);
        if(color == Colors.BLACK)
            Destroy((gameObject));
        else
            GetComponent<Renderer>().material = materials[(int)color];
    }
}
