﻿using UnityEngine;
using System.Collections;
using System;

public class TimeManager : MonoBehaviour {

    private float _startingTick = .0f;
    private float _pausedTick = .0f;

    public int TargetTime = 100;

    public string TimeElapsed {
        get {
            int currentTime = (int)Mathf.Floor(Time.time - _startingTick);
            int minutes = currentTime/60;
            int seconds = currentTime%60;
            return minutes + ":" + ((seconds < 10)?"0":"") + seconds;
        }
    }

    public void Reset() {
        _startingTick = Time.time;
        _pausedTick = .0f;
    }

    internal int ComputeTimeScore()
    {
        int currentTime = (int)Mathf.Floor(Time.time - _startingTick);
        if (TargetTime < currentTime)
            return 0;
        return (int) (1.0f/Mathf.Pow(1.0f - (TargetTime - currentTime) / (float)TargetTime, 3))*20;
    }
}
