﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

    public float offsetX = 3.0f;            //The X range of mobility
    public float speed = 3.0f;              //The speed of the animation

    private float offsetY;                  //The Y offset between platform and ball
    private const float minDist = 0.2f;     //The min value of distance between target and this, before changing target

    // Use this for initialization
    void Awake () {
	    offsetY = transform.position.y - transform.parent.position.y;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 target = transform.parent.position + Vector3.right * offsetX + Vector3.up * offsetY;
        Vector3 moveDirection = target - transform.position;

        if (moveDirection.magnitude < minDist){
            target = target + Vector3.right * offsetX;
            offsetX *= -1;
        } else {
            transform.position += moveDirection.normalized * speed * Time.deltaTime;
        }
    }
}
