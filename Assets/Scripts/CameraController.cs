﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public Transform playerFollowed;                //Reference to the player followed
    [HideInInspector] public bool freeze = false;   //If the camera is moving ?
    public Transform limitLeft;                     // The limitleft of the map
    public Transform limitRight;                    // The limitright of the map
    public float limitBottom;                       // The limitbottom of the map

    public float height { get; private set; }
    public float width { get; private set; }

    //Called everytime the camera is woken up
    void Awake() {
        height = 2f * GetComponent<Camera>().orthographicSize;
        width = height * GetComponent<Camera>().aspect;
        freeze = false;
    }
	
	// Update is called once per frame
	void Update () {
	    if (!freeze) {
	        float posX, posY;
	        if (playerFollowed.position.x - width/2.0f < limitLeft.position.x)
                posX = Mathf.Max(transform.position.x, limitLeft.position.x + width/2.0f);
            else if(playerFollowed.position.x + width/2.0f > limitRight.position.x)
                posX = Mathf.Min(transform.position.x, limitRight.position.x - width / 2.0f);
            else
	            posX = playerFollowed.position.x;
	        if (playerFollowed.position.y - height/2.0f < limitBottom)
	            posY = Mathf.Max(transform.position.y, limitBottom + height / 2.0f);
	        else {
	            posY = playerFollowed.position.y;
	        }
            transform.position = new Vector3(posX, posY, transform.position.z);
	    }
	}
}
