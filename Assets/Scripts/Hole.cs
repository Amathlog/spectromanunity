﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour {

    // Collider to kill the player if fall into a hole
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            col.gameObject.GetComponent<Player>().death();
        }
    }
}
