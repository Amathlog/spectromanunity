﻿using UnityEngine;
using System.Collections;

public class MirrorController : MonoBehaviour {

    public bool clockWise = false;      // If the mirror turns, in which way?
    public float speed = 60.0f;         // Speed of the rotation
    public bool movable = false;        // Can the mirror be pushed ?
    public float limitLeft = 3.0f;      // The limit the mirror can move from the starting position of the mirror to the left
    public float limitRight = 4.0f;     // The limit the mirror can move from the starting position of the mirror to the right
    public Transform barPrefab;               // The bar indicating the range of move available. Size on X = 1;

    private Vector3 normalVector;       // The normal vector of the reflective face
    private float minX = 0.0f;                 // Computed from limitLeft at begining
    private float maxX = 0.0f;                 // Computed from limitRight at begining
    

    // Use this for initialization
    void Start () {
        //The sprite faces left
	    normalVector = transform.right * -1.0f;
	    normalVector.Normalize();
        minX = transform.position.x - limitLeft;
        maxX = transform.position.x + limitRight;
        if (movable) {
            createBar();
        }
        else {
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.CompareTag("Photon")) {
            Projectile p = col.gameObject.GetComponent<Projectile>();
            if(Vector3.Dot(p.direction, normalVector) <= 0)
                p.direction = p.direction - 2*Vector3.Dot(p.direction, normalVector)*normalVector;
            else {
                Destroy(col.gameObject);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        normalVector = transform.right * -1.0f;
        normalVector.Normalize();
        if (transform.position.x > maxX)
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        if (transform.position.x < minX)
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
    }

    //Used byt the button to rotate the mirror
    public void rotate() {
        transform.Rotate((clockWise)?Vector3.forward:Vector3.back, speed*Time.deltaTime);
    }

    public void createBar() {
        Transform bar = Instantiate(barPrefab);
        bar.localScale = new Vector3(maxX - minX, bar.localScale.y, bar.localScale.z);
        bar.position = new Vector3(minX + (maxX - minX) / 2.0f, transform.position.y, 1);
    }

    // The Gizmo, useful to see the limit in the editor
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        if (movable) {
            Gizmos.DrawLine(transform.position + Vector3.left*limitLeft, transform.position + Vector3.right * limitRight);
        }
    }
}
