﻿using UnityEngine;
using System.Collections;

public class Star : MonoBehaviour {

    public int starScore = 10;

    private GameManager manager;

    void OnTriggerEnter2D(Collider2D col) {
        if (!col.gameObject.CompareTag("Player")) return;
        manager.AddScore(starScore);
        Destroy(gameObject);
    }

	void Start () {
	    manager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

}
