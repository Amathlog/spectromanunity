﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScreenFading : MonoBehaviour {
    public Texture2D FadeOutTexture;
    public float FadeSpeed = 1.5f;

    private int _drawDepth = -1000;
    private float _alpha = 1.0f;        
    private int _fadeDir = -1;          //Fade in(1) or fade out(-1)

    void onGUI() {
        _alpha += _fadeDir*FadeSpeed*Time.deltaTime;
        _alpha = Mathf.Clamp01(_alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, _alpha);
        GUI.depth = _drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeOutTexture);
    }

    public float BeginFade(int direction) {
        _fadeDir = direction;
        return FadeSpeed;
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) {
        BeginFade(-1);
    }
}