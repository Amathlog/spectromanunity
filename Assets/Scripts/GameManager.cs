﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private String _dataPath;                       //Contain the path to saved data
    private const int NbLevels = 5;                 //Number of levels, Constant
    private const int FirstLevel = 3;               //The number in scene settings of the first level
    private const int MainMenu = 0;                 //Scene number of the main menu
    private TimeManager _timeManager;               //The Time manager


    public static GameManager Manager;              //Reference to itself, useful to create only one instance

    public PlayerData Data;                         //Contains everything useful

    public int CurrentScene { get; set; }           //The current scene
    public bool Paused { get; set; }                //If the game is paused or not
    public GameObject PauseMenu{ get; set; }        //Reference to the pause menu
    public bool CanPause { get; set; }              // Can it be paused ?

    public int FirstLevelSceneNumber {
        get { return FirstLevel; }
    }

    public int LastLevelSceneNumber
    {
        get { return NbLevels + FirstLevel; }
    }

    public string TimeElapsed {
        get { return _timeManager.TimeElapsed; }
    }


    void Awake () {
	    if (Manager == null) {
	        DontDestroyOnLoad(gameObject);
            Manager = this;
            _dataPath = Application.persistentDataPath + "/savedData.dat";
            Data = new PlayerData(NbLevels);
            _timeManager = GetComponent<TimeManager>();
            Load();
	    } else if (Manager != this) {
            Destroy(gameObject);
        }
        Manager.CurrentScene = SceneManager.GetActiveScene().buildIndex;
        Time.timeScale = 1;

        //Managing the Pause section
        Manager.CanPause = (Manager.CurrentScene >= FirstLevel && Manager.CurrentScene <= FirstLevel + NbLevels);   // Paused only in levels
        Manager.Paused = false;                                                                                     // Cannot be on paused at start     
        if (Manager.CanPause) {
            Manager.PauseMenu = GameObject.Find("PauseMenu");                                                       // Attach the right PauseMenu
            GameObject.Find("Resume").GetComponent<Button>().onClick.AddListener(Manager.Unpause);                  // Add unpause action to the resume button
            Manager.PauseMenu.SetActive(false);                                                                     // Not active at start
        }
    }

    public void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(_dataPath);

        bf.Serialize(file, Data);
        file.Close();
    }

    public void Load() {
        if (!File.Exists(_dataPath)) return;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(_dataPath, FileMode.Open);

        Data = (PlayerData)bf.Deserialize(file);
        file.Close();
    }

    public void LoadingNewLevel(int sceneNumber) {
        if (sceneNumber > FirstLevel + NbLevels-1) {
            print("No Level after this one !");
            sceneNumber = MainMenu;
        }
        UpdtateMaxScore(CurrentScene);
        ResetUIText();
        Save();
        CurrentScene = sceneNumber;
        // If it's the first time the player load this level
        if (sceneNumber - FirstLevel + 1 > Data.LevelUnlocked)
            Data.LevelUnlocked = sceneNumber - FirstLevel + 1;
        SceneManager.LoadScene(sceneNumber);
        _timeManager = GetComponent<TimeManager>();
        _timeManager.Reset();
    }

    public void ReloadingLevel() {
        ResetUIText();
        SceneManager.LoadScene(CurrentScene);
        _timeManager = GetComponent<TimeManager>();
        _timeManager.Reset();
    }

    public int GetSceneMaxScore(int sceneNumber) {
        if (sceneNumber < FirstLevel || sceneNumber > FirstLevel + NbLevels)
            return 0;
        return (int)Data.MaxScore[sceneNumber - FirstLevel];
    }

    public void UpdtateMaxScore(int sceneNumber) {
        if (sceneNumber < FirstLevel || sceneNumber > FirstLevel + NbLevels)
            return;
        Data.CurrentScore += _timeManager.ComputeTimeScore();
        Debug.Log(_timeManager.ComputeTimeScore());
        if(Data.CurrentScore > (int)Data.MaxScore[sceneNumber - FirstLevel])
            Data.MaxScore[sceneNumber - FirstLevel] = Data.CurrentScore;
    }

    public void AddScore(int starScore) {
        Data.CurrentScore += starScore;
    }

    public void ResetMaxScore() {
        for (var i = 0; i < Data.MaxScore.Count; i++)
        {
            Data.MaxScore.Insert(i, 0);
        }
    }

    private void ResetUIText() {
        Data.CurrentScore = 0;
    }

    public void PauseAction() {
        if (CanPause) {
            if (Paused)
                Unpause();
            else {
                Pause();
            }
        }
    }

    public void Pause() {
        if (!Paused) {
            PauseMenu.SetActive(true);
            Paused = true;
            Time.timeScale = 0;
        }
    }

    public void Unpause() {
        if (Paused) {
            PauseMenu.SetActive(false);
            Paused = false;
            Time.timeScale = 1;
        }
    }

    public void ResetDataPlayer() {
        Data.Reset();
        Debug.Log(Data.ToString());
    }

}

[Serializable]
public class PlayerData {
    public ArrayList MaxScore;
    public int CurrentScore;
    public int LevelUnlocked;   //Give the maximum level unlocked.

    public PlayerData(int nb_levels) {
        MaxScore = new ArrayList(nb_levels);
        for (var i = 0; i < nb_levels; i++) {
            MaxScore.Insert(i, 0);
        }
        CurrentScore = 0;
        LevelUnlocked = 1;
    }

    public void Reset() {
        int nb_levels = MaxScore.Count;
        for (var i = 0; i < nb_levels; i++)
        {
            MaxScore[i] = 0;
        }
        CurrentScore = 0;
        LevelUnlocked = 1;
    }

    public override string ToString() {
        string res = "Player Data :\nMax scores : [ ";
        foreach (int i in MaxScore) {
            res += i + " ";
        }
        res += "]\nCurrentScore = " + CurrentScore + "\nLevelUnlocked = " + LevelUnlocked;
        return res;
    }
}
