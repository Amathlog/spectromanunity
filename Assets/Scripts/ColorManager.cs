﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ColorManager : MonoBehaviour {

    public float invincibleTime = 4.0f;                 // Time of invicibility of the player (cannot lose colors/die)
    public float flickeringTime = 0.33f;                // Used for animation of flickering (when losing color)

    private const int NB_COLORS = 3;                    // Number of primary colors, set to 3 in this game

    private Colors[] colorList;                         // List of the colors attached to the player, sorted from the first gotten to the last

    private Animator animator;                          // Reference to the animator attached to the player
    private Rigidbody2D rb2d;                           // Reference to the rigidBody2D attached to the player
    private Colors currentColor = Colors.BLACK;         // The current color of the player
    private bool invincible = false;                    // Is the player currently invincible ?
    
    // Useful for debugging, print the colorList
    private void printList() {
        string res = "[ ";
        for (int i = 0; i < NB_COLORS - 1; i++) {
            res += colorList[i];
            res += ", ";
        }
        res += colorList[NB_COLORS - 1];
        res += " ]";
        print(res);
    }

	// Use this for initialization
	void Awake () {
	    colorList = new Colors[3];
	    for (int i = 0; i < NB_COLORS; i++) {
	        colorList[i] = Colors.EMPTY;
	    }
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    //Set the color at the end of the List (aka. the first Empty slot)
    private void setColor(Colors color) {
        for (var i = 0; i < NB_COLORS; i++) {
            if (colorList[i] == Colors.EMPTY) {
                colorList[i] = color;
                break;
            } else if (colorList[i] == color) {
                //Couleur déja possédée
                break;
            }
        }
    }

    //Remove the color, useful for filters
    private void loseColor(Colors color) {
        for (var i = 0; i < NB_COLORS; i++) {
            if (colorList[i] == color) {
                colorList[i] = Colors.EMPTY;
                break;
            }
        }
        correcrionColorList();
    }

    //Remove the last color, if no color left => death.
    //Return false if dying
    private bool loseLastColor(){
        for (var i = NB_COLORS - 1; i >= 0; i--) {
            if (colorList[i] != Colors.EMPTY) {
                colorList[i] = Colors.EMPTY;
                return true;
            }
        }
        //No more color
        GetComponent<Player>().death();
        return false;
    }

    // Move the colors to the beginning of the list (in case of the first/second color is removed)
    private void correcrionColorList() {
        for (var i = 0; i < NB_COLORS - 1; i++){
            if (colorList[i] == Colors.EMPTY && colorList[i+1] != Colors.EMPTY) {
                colorList[i] = colorList[i + 1];
                colorList[i+1] = Colors.EMPTY;
            }
        }
    }

    //Additivity of colors, is not generic, need to be used carefuly to not have infinite loop
    private Colors addition(Colors c1, Colors c2) {
        if (c1 == Colors.EMPTY && c2 != Colors.EMPTY) {
            return c2;
        }
        else if (c2 == Colors.EMPTY && c1 != Colors.EMPTY) {
            return c1;
        }
        else if (c2 == Colors.EMPTY && c1 == Colors.EMPTY) {
            return Colors.BLACK;
        }
        else {
            if (c1 == Colors.RED) {
                switch (c2) {
                    case Colors.BLUE:
                        return Colors.MAGENTA;
                    case Colors.GREEN:
                        return Colors.YELLOW;
                    case Colors.CYAN:
                        return Colors.WHITE;
                    default:
                        return Colors.RED;
                }
            }
            else if (c1 == Colors.BLUE)
            {
                switch (c2)
                {
                    case Colors.RED:
                        return Colors.MAGENTA;
                    case Colors.GREEN:
                        return Colors.CYAN;
                    case Colors.YELLOW:
                        return Colors.WHITE;
                    default:
                        return Colors.BLUE;
                }
            }
            else if (c1 == Colors.GREEN) {
                switch (c2) {
                    case Colors.BLUE:
                        return Colors.CYAN;
                    case Colors.RED:
                        return Colors.YELLOW;
                    case Colors.MAGENTA:
                        return Colors.WHITE;
                    default:
                        return Colors.GREEN;
                }
            }
            else if (c1 == Colors.CYAN || c1 == Colors.MAGENTA || c1 == Colors.YELLOW) {
                return addition(c2, c1);
            }
            else {
                return c1;
            }
        }
    }

    //Add all the colors in the list to get the currentColor
    private void changeCurrentColor() {
        Colors curr = Colors.EMPTY;
        for (var i = 0; i < NB_COLORS; i++) {
            curr = addition(curr, colorList[i]);
        }
        currentColor = curr;
    }

    // Conversion Enum Colors => String
    public String getstringColor(Colors c) {
        switch (c) {
            case Colors.BLUE:
                return "Blue";
            case Colors.CYAN:
                return "Cyan";
            case Colors.MAGENTA:
                return "Magenta";
            case Colors.YELLOW:
                return "Yellow";
            case Colors.GREEN:
                return "Green";
            case Colors.RED:
                return "Red";
            case Colors.WHITE:
                return "White";
            default:
                return "";
        }
    }

    //Play the color associated animation
    private void setNewAnimation() {
        if(Mathf.Abs(rb2d.velocity.x) > 0.1)
            animator.Play("Move" + getstringColor(currentColor));
        else
            animator.Play("Idle" + getstringColor(currentColor));
    }

    // Public method called from outside to change the color and everything necessary with
    // 3 possibilities :
    // c is Red, Blue or Green and add = true : Add this color to the player
    // c is Empty or Black and add = false : Remove last color from the player
    // c is everything else and add = false : Remove the color(s) from the player
    public void changeColor(Colors c, bool add) {
        if (add)
            setColor(c);
        else {
            if (c == Colors.EMPTY || c == Colors.BLACK) {
                if (!invincible) {
                    if (loseLastColor()) {
                        invincible = true;
                        StartCoroutine(beInvincible());
                    }
                    else {
                        return;
                    }
                }
            }
            else {
                if (c == Colors.CYAN || c == Colors.GREEN || c == Colors.BLUE) {
                    loseColor(Colors.RED);
                }
                if (c == Colors.YELLOW || c == Colors.GREEN || c == Colors.RED) {
                    loseColor(Colors.BLUE);
                }
                if (c == Colors.MAGENTA || c == Colors.BLUE || c == Colors.RED) {
                    loseColor(Colors.GREEN);
                }
            }
        }
        changeCurrentColor();
        setNewAnimation();
    }

    // Return the current color
    public Colors getCurrentColor() {
        return currentColor;
    }

    //Coroutine used to be invincible
    IEnumerator beInvincible() {
        StartCoroutine(flickering());
        yield return new WaitForSeconds(invincibleTime);
        invincible = false;
    }

    //Coroutine used to flicker. Call itself until invicible is on
    IEnumerator flickering() {
        iTween.ColorTo(transform.gameObject, new Color(1, 1, 1, 0), flickeringTime);
        yield return new WaitForSeconds(flickeringTime);
        iTween.ColorTo(transform.gameObject, new Color(1, 1, 1, 1), flickeringTime);
        yield return new WaitForSeconds(flickeringTime);
        if (invincible)
            StartCoroutine(flickering());
    }

    //Return true if the color contain the other one
    public static bool isComponent(Colors source, Colors target) {
        switch (source) {
            case Colors.WHITE:
                return true;
            case Colors.CYAN:
                return (target == Colors.CYAN || target == Colors.BLUE || target == Colors.GREEN);
            case Colors.YELLOW:
                return (target == Colors.YELLOW || target == Colors.RED || target == Colors.GREEN);
            case Colors.MAGENTA:
                return (target == Colors.MAGENTA || target == Colors.BLUE || target == Colors.RED);
            default:
                return (target == source);
        }
    }

    //Return the color after Filter
    public static Colors filtering(Colors source, Colors filter) {
        if (filter == Colors.BLUE || filter == Colors.RED || filter == Colors.GREEN) {
            return (isComponent(source, filter)) ? filter : Colors.BLACK;
        }
        else if (source == Colors.WHITE) {
            return filter;
        }
        else {
            switch (filter) {
                case Colors.CYAN:
                    if (source == Colors.YELLOW || source == Colors.GREEN)
                        return Colors.GREEN;
                    else if (source == Colors.MAGENTA || source == Colors.BLUE) {
                        return Colors.BLUE;
                    }
                    break;
                case Colors.YELLOW:
                    if (source == Colors.CYAN || source == Colors.GREEN)
                        return Colors.GREEN;
                    else if (source == Colors.MAGENTA || source == Colors.RED)
                    {
                        return Colors.RED;
                    }
                    break;
                case Colors.MAGENTA:
                    if (source == Colors.YELLOW || source == Colors.RED)
                        return Colors.RED;
                    else if (source == Colors.CYAN || source == Colors.BLUE)
                    {
                        return Colors.BLUE;
                    }
                    break;
            }
            return (source == filter) ? filter : Colors.BLACK;
        }
    }
}
