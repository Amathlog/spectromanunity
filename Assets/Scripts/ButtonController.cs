﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {

    public Sprite buttonSpriteReleased;                 // Reference of the sprite of released button
    public Sprite buttonSpritePushed;                   // Reference of the sprite of pushed button
    public MirrorController[] mirrors;                  // All the mirrors impacted by the button

    private SpriteRenderer spriteRenderer;              // Reference of the renderer attached to this
    private bool pushed = false;                        // Is it pushed currently ?

	// Use this for initialization
	void Start () {
	    spriteRenderer = GetComponent<SpriteRenderer>();
	}

    void OnCollisionEnter2D(Collision2D col) {
        if (!col.gameObject.CompareTag("Player")) return;
        if (!col.gameObject.GetComponent<CharacterController2D>().StandingOn.CompareTag("Button")) return;
        pushed = true;
        spriteRenderer.sprite = buttonSpritePushed;
    }

    void OnCollisionExit2D(Collision2D col) {
        if (!col.gameObject.CompareTag("Player")) return;
        pushed = false;
        spriteRenderer.sprite = buttonSpriteReleased;
    }
	
	// Update is called once per frame
	void Update () {
        if(pushed)
	        foreach (var mirror in mirrors) {
	            mirror.rotate();
	        }
	}
}
