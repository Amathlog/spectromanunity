﻿using UnityEngine;
using System.Collections;

public class PikController : SwitchActivator
{

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            ColorManager manager = col.gameObject.GetComponent<ColorManager>();
            manager.changeColor(Colors.BLACK, false);
        }
    }

    public override void onActivation(bool state) {
        gameObject.SetActive(state);
    }
}
