﻿using UnityEngine;
using System.Collections;

public class SwitchController : MonoBehaviour {

    public Colors color;                            // Color of the switch
    public Sprite spriteOff;                        // Reference to the sprite of Switch Off
    public Sprite spriteOn;                         // Reference to the sprite of Switch On
    public bool initialState = false;               // Initial state, On or Off ?
    public SwitchActivator[] impactedObjects;       // List of all impacted objects

    private bool currentState;                      // Give the current state
    private SpriteRenderer spriteRenderer;          // Reference to the sprite renderer attached to the switch

    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        currentState = initialState;
        activation();
        changeSprite();
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (!col.gameObject.CompareTag("Photon")) return;
        if (ColorManager.isComponent(col.gameObject.GetComponent<Projectile>().color, color)) {
            currentState = !currentState;
            changeSprite();
            activation();
        }
        Destroy(col.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    //Change the sprite depending on currentSate
    private void changeSprite() {
        spriteRenderer.sprite = currentState ? spriteOn : spriteOff;
    }

    private void activation() {
        if (impactedObjects.Length <= 0) return;
        foreach (var obj in impactedObjects)
        {
            obj.onActivation(currentState);
        }
    }
}
