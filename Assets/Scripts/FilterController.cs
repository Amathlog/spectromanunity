﻿using UnityEngine;
using System.Collections;

public class FilterController : SwitchActivator
{

    public Colors color;        // Color of the filter (which one the player will keep after);

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player"))
            col.gameObject.GetComponent<ColorManager>().changeColor(color, false);
        if (col.gameObject.CompareTag("Photon")) {
            col.gameObject.GetComponent<Projectile>().filtercolor(color);
        }
    }

    public override void onActivation(bool state) {
        gameObject.SetActive(state);
    }
}
