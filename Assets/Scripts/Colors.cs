﻿public enum Colors {
    //Order : Red Green Blue Cyan Magenta Yellow White
    RED = 0,
    BLUE = 2,
    GREEN = 1,
    CYAN = 3,
    YELLOW = 5,
    MAGENTA = 4,
    WHITE = 6,
    BLACK = 7,
    EMPTY = 8
}
