﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour {

    public int nextScene = 0;           //Indicate the scene loaded after the end of the level

    void OnTriggerEnter2D(Collider2D col) {
        if (!col.gameObject.CompareTag("Player")) return;
        GameManager manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        manager.LoadingNewLevel(manager.CurrentScene + 1);
    }
}
