﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EnemyController : MonoBehaviour {

    public Transform[] waypoints;                   // Path of the enemy
    public float speed = 3.0f;                      // The speed of moving
    public bool loop = false;                       // Is the path is restarted to 0 at the end (true) or running backwards (false)
    public int startingWaypoint = 0;                // The starting point, can be set differently in the editor
    public int maxLife = 1;                         // The maximum life of the enemy
    public SwitchActivator[] impactedObjects;       // List of all impacted objects when ennemy dies
    public int scoreDeath = 10;                     // Amount earn when killing enemy
    public float timeBeforeLifeBarDispears;         // Time before the life bar will diseapear

    private int currentWaypoint;            // The position in the path
    private int increment = 1;              // Increment, useful for backwards
    private int currentLife;                // Current life, if 0 the enemy disapears
    private Transform lifeBar = null;       // Cache lifebar
    private float lifeBarTime = -1.0f;      // At which time we need to remove the bar
    private float maxWidthLifeBar = 0.0f;


	// Use this for initialization
	void Start () {
	    currentWaypoint = startingWaypoint;
	    currentLife = maxLife;
	    transform.position = waypoints[startingWaypoint].position;
        if (transform.childCount > 0)
        {
            lifeBar = transform.GetChild(0);
            lifeBar.gameObject.SetActive(false);
            maxWidthLifeBar = lifeBar.localScale.x;
        }
	}
    
    // If the enemy is shot or collide with the player
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Photon")) {
            currentLife--;
            UpdateLifeBar();
            Destroy(col.gameObject);
            if (currentLife == 0) {
                Destroy(transform.gameObject);
                GameObject.Find("GameManager").GetComponent<GameManager>().AddScore(scoreDeath);
                foreach (var obj in impactedObjects) {
                    obj.onActivation(true);
                }
            }
        } else if (col.gameObject.CompareTag("Player")) {
            ColorManager manager = col.gameObject.GetComponent<ColorManager>();
            manager.changeColor(Colors.BLACK, false);
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (currentWaypoint >= waypoints.Length) {
	        if (loop) {
	            currentWaypoint = 0;
	        }
	        else {
	            currentWaypoint = waypoints.Length - 2;
	            increment = -1;
	        }
	    } else if (currentWaypoint < 0) {
	        currentWaypoint = 1;
	        increment = 1;
	    }
	    else {
            move();
        }
        if (lifeBar != null && lifeBarTime != -1.0f & Time.time > lifeBarTime)
        {
            lifeBar.gameObject.SetActive(false);
        }
	}

    // Moving function
    private void move() {
        Vector3 target = waypoints[currentWaypoint].position;
        Vector3 moveDirection = target - transform.position;
        
        if (moveDirection.magnitude < 0.5) {
            currentWaypoint += increment;
        }
        else {
            transform.position += moveDirection.normalized*speed*Time.fixedDeltaTime;
        }
    }

    private void UpdateLifeBar()
    {
        if (lifeBar == null)
            return;
            
        if (currentLife == 0)
            return;

        lifeBar.gameObject.SetActive(true);

        lifeBarTime = Time.time + timeBeforeLifeBarDispears;
        float ratio = (float)currentLife / maxLife;

        Vector3 scale = lifeBar.localScale;
        scale.x = maxWidthLifeBar * ratio;
        lifeBar.localScale = scale;
    }

    // The Gizmo, useful to see the path in the editor
    public void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        for (int i = 0; i < waypoints.Length - 1; i++) {
            Gizmos.DrawLine(waypoints[i].position, waypoints[i+1].position);
        }
    }
}


