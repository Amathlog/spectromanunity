﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelButtonGenerator : MonoBehaviour {

    public GameObject LevelBundle;
    private GameManager Manager;
    public GameObject CanvasGameObject;

	// Use this for initialization
	void Start () {
	    Manager = GameObject.Find("GameManager").GetComponent<GameManager>();
	    Generation();
	}

    private void Generation() {
        int firstLevel = Manager.FirstLevelSceneNumber;
        int currLevel = 1;
        Debug.Log(Manager.Data.ToString());
        for (int i = firstLevel; i <= Manager.LastLevelSceneNumber; ++i)
        {
            GameObject newLevel = Instantiate(LevelBundle);     
            newLevel.transform.SetParent(CanvasGameObject.transform);
            newLevel.transform.localScale = new Vector3(1, 1, 1);
            newLevel.GetComponentInChildren<Button>().GetComponentInChildren<Text>().text = "Level " + currLevel;
            if (Manager.Data.LevelUnlocked >= currLevel)
            {
                var i1 = i;
                newLevel.GetComponentInChildren<Button>().onClick.AddListener(delegate { Manager.LoadingNewLevel(i1); });
                newLevel.GetComponentsInChildren<Text>()[1].text = "Max Score " + Manager.Data.MaxScore[currLevel - 1];
            }
            else
            {
                ColorBlock auxColor = newLevel.GetComponentInChildren<Button>().colors;
                auxColor.normalColor = Color.grey;
                newLevel.GetComponentInChildren<Button>().enabled = false;
                newLevel.GetComponentInChildren<Button>().colors = auxColor;
            }
            currLevel++;
        }
    }
}
