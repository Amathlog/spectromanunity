﻿using UnityEngine;
using System.Collections;

public class TestLoseColor : MonoBehaviour {

    public Colors color;

	// Use this for initialization
	void Start () {
	
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        col.gameObject.GetComponent<ColorManager>().changeColor(color, false);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
