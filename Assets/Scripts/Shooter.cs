﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    public float timeBetweenShoot = 1.0f;               // Reloading time, time between 2 shots
    //Order : Red Green Blue Cyan Magenta Yellow White
    public Transform[] projectiles;                     // Prefabs of all the projectiles possible
    

    private Player player;              // Reference to the controller attached to the player 
    private bool loaded = true;                         // If the gun loaded (can I fire ?)
    private ColorManager colorManager;                  // Reference to the colorMaager attached to the player
    private bool fireTrigger = false;                   // When fire is triggered

    // Use this for initialization
    void Awake () {
        player = GetComponent<Player>();
	    colorManager = GetComponent<ColorManager>();
	}

    // Coroutine for reloading
    IEnumerator reloading() {
        yield return new WaitForSeconds(timeBetweenShoot);
        loaded = true;
    }
	
	// Update is called once per frame
	void Update () {
	    if (fireTrigger && loaded && colorManager.getCurrentColor() != Colors.BLACK) {
	        fireTrigger = false;
	        loaded = false;
	        Transform projectile =
	            Instantiate(projectiles[(int) colorManager.getCurrentColor()],
	                transform.position + Vector3.right*(player.isFacingRight ? 1 : -1), transform.rotation) as Transform;
	        if (projectile)
	            projectile.GetComponent<Projectile>().direction = Vector3.right*(player.isFacingRight ? 1 : -1);
	        StartCoroutine(reloading());
	    }
	    else {
	        fireTrigger = false;
	    }
	}

    public void fire() {
        fireTrigger = true;
    }
}
