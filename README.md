# Spectroman #

This is the repository of the game **Spectroman**, developed for a school project by 5 students(including me) of **ENSIMAG** (Grenoble - France) with Javascript language and re-adapted for Unity by me.
The objective was to get some experience with Unity on a project already done, and to improve it with new features.
The game is coded with **Unity 5.3** and in **C#**. All graphics are from the assert store or drew by us during the project.

### The game ###

This game was developed for "La fête de la science" in October 2015 in Grenoble. The theme was the **Light**. For this game, we chose to focus on the colors and some property of the light such as reflection, additive colors and filters.
It is a **2D puzzle-platform game** cut in small levels.
The idea behind adapt it to Unity3D was to use the powerful engine provided by Unity and the UI for level design.

![Capture d'écran 2015-12-22 18.08.25.png](https://bitbucket.org/repo/q45zzM/images/1844157406-Capture%20d'%C3%A9cran%202015-12-22%2018.08.25.png)

### What is already done ? ###

Currently, all the gameplay features which were present in the original game are coded and functional. Nevertheless, it needs some finition such as Camera follow, jump control and move control. Yet the game is **fully playable**.

### What need to be done ###

* Introduction (story and cutscene)
* Start Menu/Level Menu/End of level screen
* Audio
* Levels (Only six are yet implemented)
* New animations (or improving the already existing ones)
* Graphic improvement (background etc...)
* Gameplay finition
* Touch support

### Who do I talk to? ###

* Repo owner or admin

### How to use it? ###

This repo is linked with **Unity Cloud Build**, allowing the game to be compiled to a WebGL ressource, in order to play in your own browser :

* https://build.cloud.unity3d.com/share/-1tyuQjgTe/

The build for other platforms is also available :

* Linux : https://build.cloud.unity3d.com/share/b1KuVQslTl/
* Windows : https://build.cloud.unity3d.com/share/WJFlvmoxpx/
* MacOS : https://build.cloud.unity3d.com/share/-J2twQjlTg/

Controls are :

 * **Left/Right Arrow** : Move
 * **Up Arrow** : Jump
 * **Down Arrow** : Change color (Only on colored platforms)
 * **Space** : Fire (Only if you have at least one color)